#!/bin/bash
if [ "$1" != "" ]; then
    echo "Grabbing repository..."
    git clone "$1"
else
    echo "Fatal: Repostitory not found."
    exit
fi

